#include <string>
#include <conio.h>
#include "Sensor.h"
#include <sqlite3.h> 
//-----
//���������
//-------
const int num_of_sensor = 3;//���������� ��������

sqlite3 *db_base = 0;

void main(void)
{  
    setlocale(LC_ALL, "Russian");

    using std::cout;
    using std::cin;
    using std::endl;

    //------
    //�������� ���� ������
    //-------
    
    Create_Base();

    //------
    //������������� ��������
    //-------
    Tem_sensor * sensors[num_of_sensor];
     
    //������ ��������  
    std::string mod_sensor[num_of_sensor] =
    {
        {"TSP-1199"},
        {"TXA-1199(Exd)"},
        { "TF44"},
    };
  
    //����������    
    coordinates c_s[num_of_sensor]
    {
        {25.35,64.78},
        {32,47},
        {67.09,100.0},
    };
 
    //���� ��������� �������   
    install_date i_s[num_of_sensor]
    {
        {1,2,1998},
        {3,4,1996},
        {12,3,1980},
    };

    cout << "The list of sensors: \n";

    for (int count_sens = 0; count_sens < num_of_sensor; count_sens++)
    {
        sensors[count_sens] = new Tem_sensor(mod_sensor[count_sens],
            c_s[count_sens], i_s[count_sens]);

        cout << "Sensor " << (count_sens+1)
            << ": " << mod_sensor[count_sens] << endl;
    }

    //-------
    //������������� ��������� ��������
    //-------
    for (int count_sens = 0; count_sens < num_of_sensor; count_sens++)
        Generator(sensors[count_sens]);

    cout << "Please select the sensor number from the list provided.\n";
    
    int number;
    cin >> number;
    number--;
    cin.get();

    cout << "Please enter the range in format: YYYY - MM - DD HH : MM:SS\n";

    string time_start;
    string time_finish;

    cout << "Date_time start: ";
    getline(cin, time_start); 
    cout << "Date_time end: ";
    getline(cin, time_finish);

    /*Show*/
    sensors[number]->Show();
    cout << "Max: " << sensors[number]->Max(time_start, time_finish)<<endl;
    cout << "AVG: " << sensors[number]->Middle(time_start, time_finish)<<endl;
    cout << "Min: " << sensors[number]->Min(time_start, time_finish)<<endl;

    //-------
    //������������ ������
    //-------
    for (int i = 0; i<num_of_sensor; ++i)
    {
        delete sensors[i];
    }
  
    _getch();
}
